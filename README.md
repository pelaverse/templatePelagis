
<!-- README.md is generated from README.Rmd. Please edit that file -->

# templatePelagis

## Installation

You can install the released version of templatePelagis with :

``` r
remotes::install_gitlab(
  host = "https://gitlab.univ-lr.fr",
  repo = "pelaverse/templatePelagis"
)
```

## To create a new presentation

To use templatePelagis from RStudio, you can access the templates
through `File -> New File -> R Markdown`

``` r
knitr::include_graphics("man/figures/new_presentation.png")
```

<img src="man/figures/new_presentation.png" width="50%" height="50%" />
