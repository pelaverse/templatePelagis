---
title: "Presentation d'une analyse CDS et krigeage (ex: CHESPP SAMM)"
subtitle: "avec geffaeR"  
author: "Mathieu Genu"
date: '`r Sys.Date()`'
output:
  xaringan::moon_reader:
    lib_dir: ressources/libs
    css: ["ressources/css/font.css","ressources/css/pelagis.css"]
    yolo: false
    seal: false
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---

```{r setup, include=FALSE}
library(tidyverse)
library(xaringanthemer)
library(DT)
library(mapview)
library(sf)
library(data.tree)
library(lubridate)
library(scales)
```


class: inverse, center, middle

# Analyse CDS et Krigeage avec geffaeR

```{r out.width='25%', echo = F}
knitr::include_graphics("ressources/img/hex_sticker_geffaeR.png")
```

Mathieu Genu & Matthieu Authier

`r lubridate::dmy(Sys.Date())`

---
## Contexte 

### A quoi sert le package geffaeR ?
